from requests import get
from pandas import read_csv, read_json
from numpy import array

class dataset():
    def prepare_data(this,
                     data_source,
                     filename,
                     file_delimiter,
                     likert_data_cols_start):
        
        this.data_requested = get(data_source)

        with open(filename, 'wb') as f:
            f.write(this.data_requested.content)

        df = read_csv(filename, delimiter=file_delimiter, encoding="utf-8")

        groups = array(df.values[1:,1:likert_data_cols_start], dtype=int) 
        this.data = array(df.values[1:,likert_data_cols_start:], dtype=int)

        _, groups = groups.shape
        this.rows, this.cols = this.data.shape
        
        return {"data": this.data, "rows": this.rows, "cols": this.cols}

if __name__ == "__main__":
    data_source = "https://raw.githubusercontent.com/tjkemp/likert-clusters/master/data/yle-election-2015-clean.csv"
    filename = "data/yle-election-2015-clean.csv"
    file_delimiter = ";"
    likert_data_cols_start = 8
    
    dataset = dataset()
    dataset.prepare_data(data_source, filename, file_delimiter, likert_data_cols_start)