# Ajout du dossier où est le script au path (nécessaire via Jupyter lab)
import sys
sys.path.append("./")

print(sys.path)

# Import du script à tester
import prepare_data_likert

# Test de démo

def add(a, b):
    return a + b

def test_add_returns_2_for_1_plus_1():
    expected = 2
    actual = add(1, 1)
    assert(expected == actual)

# Test du script standalone

def test_prepare_data_returns_a_dictionnary():
    dataset = prepare_data_likert.dataset()
    data_source = "https://raw.githubusercontent.com/tjkemp/likert-clusters/master/data/yle-election-2015-clean.csv"
    filename = "data/yle-election-2015-clean.csv"
    file_delimiter = ";"
    likert_data_cols_start = 8
    
    expected = type(dict())
    actual = type(dataset.prepare_data(data_source,
    filename,
    file_delimiter,
    likert_data_cols_start))
    
    assert(expected == actual)