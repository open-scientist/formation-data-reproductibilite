namespace="namespace"

email="nom@unistra.fr"
name="NOM Prénom"

git remote rm origin
git remote rm upstream
git remote add upstream https://git.unistra.fr/bohuon/formation-data-reproductibilite
git remote add origin https://git.unistra.fr/"$namespace"/formation-data-reproductibilite

git config --global user.email "$email"
git config --global user.name "$name"

git config --global credential.helper 'cache --timeout=36000'
