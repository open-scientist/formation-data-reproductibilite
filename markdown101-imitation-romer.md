# Endogenous Technological Change 

---

Paul M. Romer

_University of Chicago_

>Growth in this model is driven by technological change that arises from intentional investment decisions made by profit-maximizing agents. The distinguishing feature of the technology as an input is that it is neither a conventional good nor a public good; it is a non- rival, partially excludable good. Because of the nonconvexity in- troduced by a nonrival good, price-taking competition cannot be supported. Instead, the equilibrium is one with monopolistic compe- tition. The main conclusions are that the stock of human capital determines the rate of growth, that too little human capital is de- voted to research in equilibrium, that integration into world markets will increase growth rates, and that having a large population is not sufficient to generate growth.

## Introduction

Output per hour worked in the United States today is 10 times as valuable as output per hour worked 100 years ago (Maddison 1982). In the 1950s, economists attributed almost all the change in output per hour worked to technological change (Abramovitz 1956; Ken- drick 1956; Solow 1957). Subsequent analysis raised our estimates of 